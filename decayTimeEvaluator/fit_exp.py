import numpy as np
import scipy
import matplotlib.pyplot as plt
import math
from scipy.optimize import least_squares


# function creating a single decay signal value
def create_decay_signal(a_array, t):
    """ 
    Intensity calculator according to (1)

    Args:
        a_array (np.array, len=3): parameter vector
        t: parameter t
    Returns:
        float: funciton value
    """
    result = a_array[0] + a_array[1] * np.exp(-a_array[2] * t)
    return result

def fit_data(filename):
    time_series_array = np.loadtxt(filename)
    
    Bvec = []
    Tvec = []
    a2vec = []
    
    for s in time_series_array:
        Bvec.append(s[0])
        Tvec.append(s[1])
        
        s = s[2:]
        t_array = np.linspace(0, 2, 500)
        probe_len = s.shape[0] // 100
        a0 = np.mean(s[-probe_len:])
        a1 = np.mean([s[:probe_len]]) - a0
        a0, a1
        
        e = math.exp(1)
        index = probe_len//2
        while (np.mean(s[index-probe_len//2:index+probe_len//2]) - a0) > a1 / e:
            index = index + 1
        t_1_e = index / 500 * 2
        a2 = 1/t_1_e
        
        a_initial=(a0, a1, 2*a2)
        fit_func = lambda a: s - create_decay_signal(a, t_array)
        opt_result=least_squares(fit_func, a_initial)
        a_opt = opt_result.x
        a2vec.append(a_opt[2])
        
        
    return {'B': Bvec, 'T': Tvec, 'a2': a2vec}

if __name__ == '__main__':
    [o.fork() for (o,i) in [(__import__('os'), __import__('itertools'))] for x in i.repeat(0)]