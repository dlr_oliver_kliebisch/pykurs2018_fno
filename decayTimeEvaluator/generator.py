# -*- coding: utf-8 -*-

# we require numpy, math, and matplotlib.pyplot
import numpy as np
import math
import os
import configparser


# short function with a descriptive name and a signle purpose
def add_gaussian_noise(time_series, noise_sigma):
    """
    Add normally distributed noise to a time series

    Args:
        time_series(numpy.array, float32): time series
        noise_sigma (float): sigma of zero-centered noise
    """
    noise = np.random.normal(0, noise_sigma, size=time_series.shape)
    return time_series + noise

# function creating a noisy decay signal
def create_decay_time_series(a_array, noise_sigma, t_linspace):
    """ 
    Time series calculator according to eq (1).

    Args:
        a_array (np.array, len=3): parameter vector
        noise_sigma (float): sigma of gaussian noise
        t_linspace: (tuple, len=3): t values, see np.linspace parameters 
    Returns:
        np.array: function values 
    """
    t_array = np.linspace(*t_linspace)
    result_array = a_array[0] + a_array[1] * np.exp(-a_array[2] * t_array)
    noisy_result_array = add_gaussian_noise(result_array, noise_sigma)
    return t_array, noisy_result_array

# generating a set of simulated measuements
def measurement_generator(a_mean, a_sigma, B_linspace, T_linspace, t_linspace,
                          noise_sigma):
    """ Simplified simulation of decay measurements
        Args:
            a_mean (tuple, len=3): mean of parameter distribution
            a_sigma (tuple, len=3): sigma of parameter distribution
            B_linspace(tuple, len=3): B interval definition, linspace syntax
            T_linspace(tuple, len=3): T interval definition, linspace syntax
            t_linspace(tuple, len=3): t interval definition, linspace syntax
        Yields:
            tuple (b, t, signal): parameters and simulated signal
        """
    B_mean = (B_linspace[0] + B_linspace[1]) / 2
    T_mean = (T_linspace[0] + T_linspace[1]) / 2
    a_array = np.zeros(3)
    B_values = np.linspace(*B_linspace)
    np.random.shuffle(B_values)
    for B in B_values:
        delta_B = B - B_mean
        T_values = np.linspace(*T_linspace)
        np.random.shuffle(T_values)
        for T in T_values:
            delta_T = T - T_mean
            a_array = np.random.normal(a_mean, a_sigma)
            a_array[2] += math.exp(-(delta_T * delta_T + delta_B * delta_B))
            yield (B, T,
                   create_decay_time_series(a_array, noise_sigma, t_linspace))

# create text file containing measurements
def create_measurement_file(file_name, a_mean, a_sigma, B_linspace, T_linspace,
                            t_linspace, noise_sigma):
    """ 
    Simplified simulation of decay measurements
    Args:
        a_mean (tuple, len=3): mean of parameter distribution
        a_sigma (tuple, len=3): sigma of parameter distribution
        B_linspace(tuple, len=3): B interval definition, linspace syntax
        T_linspace(tuple, len=3): T interval definition, linspace syntax
        t_linspace(tuple, len=3): t interval definition, linspace syntax
    """
    measurements = measurement_generator(a_mean, a_sigma, B_linspace,
                                         T_linspace, t_linspace, noise_sigma)

    with open(file_name, 'w') as out_file: #with generates a "block"
        for m in measurements:
            out_file.write('{} {} '.format(m[0], m[1]))
            for val in m[2][1]:
                out_file.write('{} '.format(val))
            out_file.write('\n')

        #create ini file
    c = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    c.add_section('parameters')
    c['parameters']['file_name'] = file_name
    c['parameters']['a_mean'] = str(a_mean)
    c['parameters']['a_sigma'] = str(a_sigma)
    c['parameters']['_b_linspace'] = str(B_linspace)
    c['parameters']['_t_linspace'] = str(T_linspace)
    c['parameters']['t_linspace'] = str(t_linspace)
    c['parameters']['noise_sigma'] = str(noise_sigma)
    
    with open(os.path.splitext(os.path.basename(file_name))[0] + '.ini', 'w') as out_file:
        c.write(out_file)

if __name__ == "__main__":
    # example use for batch mode:
    ## create a test file
    create_measurement_file(
        'simulation.txt',
        a_mean=(2, 3, 2),
        a_sigma=(.1, .15, .05),
        B_linspace=(0, 3, 31),
        T_linspace=(100, 300, 21),
        t_linspace=(0, 2, 500),
        noise_sigma=.1)