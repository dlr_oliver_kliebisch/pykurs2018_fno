# source: http://pythonhosted.org/an_example_pypi_project/setuptools.html
import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "decayTimeEvaluator",
    version = "0.0.1",
    author = "John Doe",
    author_email = "JohnDoe@cloudbasedusersurveillance.com",
    description = ("A very simple setup.py file"),
    license = "Public domain",
    keywords = "Pykurs package",
    url = "http://myUrl.com",
    packages=['decayTimeEvaluator'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Demonstration",
        "License :: GPL License",
    ],
)
