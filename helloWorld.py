#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 10:50:41 2018

@author: pyoneer
"""
from decayTimeEvaluator.generator import create_measurement_file
from decayTimeEvaluator.fit_exp import fit_data
# example use for batch mode:
## create a test file
create_measurement_file(
    'simulation.txt',
    a_mean=(2, 3, 2),
    a_sigma=(.1, .15, .05),
    B_linspace=(0, 3, 31),
    T_linspace=(100, 300, 21),
    t_linspace=(0, 2, 500),
    noise_sigma=.1)

res = fit_data('simulation.txt')